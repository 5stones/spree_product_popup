Spree::Core::Engine.routes.draw do
  # Add your extension routes here

  get 'spree-product-popup/dismissed/:id',  to: 'product_popups#dismissed_popup'
  post 'spree-product-popup/dismissed/:id', to: 'product_popups#dismissed_popup'

  post 'spree-product-popup/populate/', to: 'product_popups#populate'

  namespace :admin, path: Spree.admin_path do

    delete 'product_popups/:id/image', to: 'product_popups#remove_image', as: 'delete_product_popup_image'

    resources :product_popups
  end
end
