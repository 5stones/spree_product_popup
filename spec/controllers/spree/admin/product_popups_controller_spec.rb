require 'spec_helper'

descrive Spree::Admin::ProductPopupsController do

  context '#index' do

    it 'can list active popups' do
      active_popup = create(:product_popup, effective_date: DateTime.now - 10.days)
      past_popup = create(:product, expiration_date: DateTime.now - 10.days)
      future_popup = create(:product, effective_date: DateTime.now + 10.days)

      spree_get :index, active: true

      expect(assign[:collection]).not_to_be_empty
      expect(assign[:collection]).to include(active_popup)
      expect(assign[:collection]).not_to include(past_popup)
      expect(assign[:collection]).not_to include(future_popup)
    end

    it 'can list future popups' do
      active_popup = create(:product_popup, effective_date: DateTime.now - 10.days)
      past_popup = create(:product, expiration_date: DateTime.now - 10.days)
      future_popup = create(:product, effective_date: DateTime.now + 10.days)

      spree_get :index, future: true

      expect(assign[:collection]).not_to_be_empty
      expect(assign[:collection]).not_to include(active_popup)
      expect(assign[:collection]).not_to include(past_popup)
      expect(assign[:collection]).to include(future_popup)
    end

    it 'can list expired popups' do
      active_popup = create(:product_popup, effective_date: DateTime.now - 10.days)
      past_popup = create(:product, expiration_date: DateTime.now - 10.days)
      future_popup = create(:product, effective_date: DateTime.now + 10.days)

      spree_get :index, expired: true

      expect(assign[:collection]).not_to_be_empty
      expect(assign[:collection]).not_to include(active_popup)
      expect(assign[:collection]).to include(past_popup)
      expect(assign[:collection]).not_to include(future_popup)
    end
  end
end
