Deface::Override.new(virtual_path: 'spree/layouts/spree_application',
  name: 'insert_product_popup',
  insert_top: "body[data-hook='body']",
  text: "<%= render partial: 'spree/frontend/product_popups/product_popup' %>"
)
