Deface::Override.new(virtual_path: 'spree/admin/shared/sub_menu/_product',
  name: 'add_admin_link',
  insert_bottom: "ul[data-hook='admin_product_sub_tabs']",
  text: "<%= tab :product_popups %>"
)
