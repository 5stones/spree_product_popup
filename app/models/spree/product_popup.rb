class Spree::ProductPopup < ActiveRecord::Base

  scope :active, -> {
    where('effective_date <= ? and expiration_date >= ?',Time.current.strftime("%Y-%m-%d"),Time.current.strftime("%Y-%m-%d"))
    .or(where('effective_date is null and expiration_date >= ?', Time.current.strftime("%y-%m-%d")))
    .or(where('expiration_date is null and effective_date <= ?', Time.current.strftime("%y-%m-%d")))
  }
  scope :past, -> { where('expiration_date < ?', Time.current.strftime("%Y-%m-%d")) }
  scope :future, -> { where('effective_date > ?', Time.current.strftime("%Y-%m-%d")) }

  # Model Fields:
  # url
  belongs_to :variant, class_name: 'Spree::Variant'
  has_one :image, as: :viewable, dependent: :destroy, class_name: 'Spree::Image'
  accepts_nested_attributes_for :image
  delegate :name, to: :variant
  # effective_date
  # expiration_date
  # custom_copy
  # title
  # confirm_text
  # decline_text

  validates :variant, presence: true
  validates :match_url, presence: true
  validates_uniqueness_of :match_url
  validate :verify_date_range

  before_save :normalize_url

  def verify_date_range
    if (self.effective_date && self.expiration_date)
      errors.add(:base, 'The specified date range is invalid. Expiration date must be on or after the effective date') unless self.effective_date <= self.expiration_date
    end
  end

  def active?
    return can_display?() && (effective_date.nil? || effective_date <= Time.current.beginning_of_day) && (expiration_date.nil? || expiration_date >= Time.current.end_of_day)
  end

  def can_display?
    # Product Popups are designed to be a one-click add to cart experience.
    # If there is a reason the product can't be added to the cart--such as when
    # it's out of stock, or unavailable in a particular region-- then the popup
    # should not be displayed
    return (self.variant.price_in(Spree::Config[:currency]) && !self.variant.price.nil?) && (self.variant.can_supply?)
  end

  def normalize_url
    self.match_url.slice!(0) if self.match_url.starts_with?('/')
  end
end
