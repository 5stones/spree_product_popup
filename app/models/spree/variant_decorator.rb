Spree::Variant.class_eval do
  has_many :product_popups, class_name: "Spree::ProductPopup"

  def name_and_sku
    sku.blank? ? name : "#{sku} - #{name}"
  end
end
