Spree::FrontendHelper.module_eval do

  # Display the popup's image on the backend editor. Default to the variant's
  # image if a custom image is not defined
  def product_popup_image(popup)

    if popup.image
      options = {}
      #   style: 'max-width: 100%;'
      # }
      options[:alt] = popup.image.alt ? popup.image.alt : popup.variant.name
      create_product_image_tag(popup.image, popup, options, 'large')
    else
      large_image(popup.variant)
    end

  end
end
