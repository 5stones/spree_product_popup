Spree::StoreController.class_eval do

  before_action :find_product_popup

  def find_product_popup


    # NOTE: We depend on the order to determine whether or not to show the

    url_path = request.path
    url_path.slice!(0) if request.path.starts_with?('/')
    found_popup = find_popup_for_path(url_path.split('/'))
    # Remove the leading '/' and split the remaining path

    @product_popup = found_popup unless(found_popup.blank? || order_contains_item?(found_popup.variant))
    # @product_popup = found_popup.blank? && order_contains_item?(found_popup.variant) ? nil : found_popup
    # If the order already contains the item we're trying to pitch, return nil and don't show a popup
  end

  def find_popup_for_path(path_tokens)

    # Popups are designed to match URL paths beginning with the most specific match
    #
    # This method accepts a tokenized version of the full path for the current url,
    # and then looks for a match. If no match is found, the last token is popped from
    # the array, and then we try again.
    #
    # This process continues until we either find a matching popup, or run out of
    # tokens

    popup = Spree::ProductPopup.active.find_by(match_url: "#{path_tokens.join('/')}")

    if ( (popup.nil? || !(popup.can_display?)) && !(path_tokens.empty?) )
      path_tokens.pop
      popup = find_popup_for_path(path_tokens)
    end

    return popup
  end

  def order_contains_item?(variant)
    order = current_order
    return order.blank? ? false : current_order.line_items.where(variant_id: variant.id).any?
  end
end
