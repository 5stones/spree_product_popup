module Spree
  class ProductPopupsController < Spree::StoreController

    def dismissed_popup
      if request.method == "GET"
        respond_to do |format|
          format.json { render json: session["dismissed_product_popup_#{params[:id]}"] ? true : false }
          # NOTE:   The popup ID is included in the session key to allow a different popup to appear on
          #         another page. If you want to display only one product popup at a time, you can override
          #         this behavior in your project.
        end
      else
        session["dismissed_product_popup_#{params[:id]}"] = true # TODO Fix this to match above. This is deliberate to facilitate testing
      end
    end

    def populate

      order    = current_order(create_order_if_necessary: true)
      variant  = Spree::Variant.find(params[:variant_id])
      quantity = params[:quantity].to_i
      options  = params[:options] || {}

      # 2,147,483,647 is crazy. See issue #2695.
      if quantity.between?(1, 2_147_483_647)
        begin
          order.contents.add(variant, quantity, options)
        rescue ActiveRecord::RecordInvalid => e
          error = e.record.errors.full_messages.join(", ")
        end
      else
        error = Spree.t(:please_enter_reasonable_quantity)
      end

      if error
        flash[:error] = error
      else
        session["dismissed_product_popup_#{params[:product_popup_id]}"] = true
        flash[:success] = "#{variant.product.name} has been added to your cart"
        #
        # respond_with(order) do |format|
        #   format.html { redirect_back_or_default(cart_path) }
        # end
      end

      redirect_back_or_default(spree.root_path)
    end

  end
end
