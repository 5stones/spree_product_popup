module Spree
  module Admin
    class ProductPopupsController < ResourceController

      def index
        @show_expired = params[:expired] || params[:expired] === "true" ? true : false
        @show_future = params[:future] || params[:future] === "true" ? true : false
        @show_active = params[:active] || params[:active] === "true" || !(@show_expired || @show_future) ? true : false

        if @show_expired
          @product_popups = Spree::ProductPopup.past.order("effective_date DESC")
        elsif @show_future
          @product_popups = Spree::ProductPopup.future.order("effective_date ASC")
        else
          @product_popups = Spree::ProductPopup.active.order("effective_date ASC")
        end
      end

      def update
        begin
          @product_popup.update_attributes(product_popup_params)
        rescue => e
          Rails.logger.error(error: e, error_message: e.message, error_trace: e.backtrace.join('\n'))
          flash[:error] = e.message
        end
        redirect_to edit_admin_product_popup_path(@product_popup)
      end

      def location_after_save
        edit_admin_product_popup_path(@product_popup)
      end

      def remove_image
        @product_popup = Spree::ProductPopup.find(params[:id])
        @product_popup.image.destroy
        @product_popup.save

        redirect_to edit_admin_product_popup_path(@product_popup)
      end

      def product_popup_params

        params[:product_popup].delete(:image_attributes) unless params[:product_popup][:image_attributes][:attachment]

        params.require(:product_popup).permit(
          [
            :variant_id,
            :effective_date,
            :expiration_date,
            :match_url,
            :headline,
            :confirm_text,
            :decline_text,
            :custom_copy,
            image_attributes: [
                :alt,
                :attachment
            ]
          ]
        )
      end

    end # END class
  end
end
