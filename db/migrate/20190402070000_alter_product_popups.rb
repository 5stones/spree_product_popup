class AlterProductPopups < ActiveRecord::Migration
  def self.up

    # Add additional customization options for the popup headline/button text
    add_column :spree_product_popups, :headline, :string
    add_column :spree_product_popups, :decline_text, :string, limit: 15
    add_column :spree_product_popups, :confirm_text, :string, limit: 15
    add_reference :spree_product_popups, :image

    # Modify ProductPopup to use Spree::Variant over Spree::Product

      # Add the new variant reference first
      add_reference :spree_product_popups, :variant
      add_index :spree_product_popups, :variant_id, unique: true
      add_foreign_key :spree_product_popups, :spree_variants, column: :variant_id

      # Update existing popups add master variant references for the currently selected product
      Spree::ProductPopup.all.find_in_batches do |batch|
        batch.each do |popup|
          popup.variant_id = Spree::Variant.find_by(is_master: 1, product_id: popup.product_id).id
        end
      end

      # Remove the old product reference
      remove_foreign_key :spree_product_popups, :spree_products
      remove_index :spree_product_popups, :product_id if index_exists?(:spree_product_popups, :product_id)
      remove_reference :spree_product_popups, :product
  end

  def self.down
    # Remove additional customization options for the popup headline/button text
    remove_column :spree_product_popups, :headline
    remove_column :spree_product_popups, :decline_text
    remove_column :spree_product_popups, :confirm_text
    remove_reference :spree_product_popups, :image

    # Rollback ProductPopup to use Spree::Product over Spree::Variant

      # Add the product reference first
      add_reference :spree_product_popups, :product
      add_index :spree_product_popups, :product_id, unique: true
      add_foreign_key :spree_product_popups, :spree_products, column: :product_id

      # Update existing popups add product references for the currently selected variant
      Spree::ProductPopup.all.find_in_batches do |batch|
        batch.each do |popup|
          popup.product_id = Spree::Variant.find(variant_id).product_id
        end
      end

      # Remove the variant reference
      remove_foreign_key :spree_product_popups, :spree_variants
      remove_index :spree_product_popups, :variant_id
      remove_reference :spree_product_popups, :variant
  end
end
