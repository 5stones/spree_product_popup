class CreateProductPopups < ActiveRecord::Migration
  def self.up
    create_table :spree_product_popups do |t|
      t.references :product
      t.date :effective_date
      t.date :expiration_date
      t.text :custom_copy, limit: 2500
      t.string :match_url
      t.timestamps
    end

    add_index :spree_product_popups, :id, unique: true
    add_index :spree_product_popups, :match_url, unique: true
    add_foreign_key :spree_product_popups, :spree_products, column: :product_id
  end

  def self.down
    drop_table :spree_product_popups
  end
end
